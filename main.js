const output = document.getElementById('output');

const quickSort = (array) => {
  if (array.length <= 1) {
    return array;
  }

  let pivot = array[array.length - 1];

  let left = [];
  let right = [];

  for (let i = 0; i < array.length - 1; i++) {
    if (array[i] < pivot) {
      left.push(array[i]);
    } else {
      right.push(array[i]);
    }
  }

  if (left.length > 0 && right.length > 0) {
    return [...quickSort(left), pivot, ...quickSort(right)];
  } else if (left.length > 0) {
    return [...quickSort(left), pivot];
  } else {
    return [pivot, ...quickSort(right)];
  }
}

output.innerText = quickSort(array = [1, 4, 2, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92, 3]);
